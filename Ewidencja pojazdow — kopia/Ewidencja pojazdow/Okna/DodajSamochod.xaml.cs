﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for DodajSamochod.xaml
    /// </summary>
    public partial class DodajSamochod : Window
    {
        public DodajSamochod()
        {
            InitializeComponent();
            FillCb();
            SamochodyWBazieLB.Items.Clear();
            FillLB();
        }

        private Klasy.DodajSamocho dodajSamochod = new Klasy.DodajSamocho();

        private void DodajBT_Click(object sender, RoutedEventArgs e)
        {
            dodajSamochod.MarkaLocal = MarkaTB.Text;
            dodajSamochod.ModelLocal = ModelTB.Text;
            Wstaw();
            SamochodyWBazieLB.Items.Clear();
            FillLB();
        }

        private void FillCb()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //var _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                SqlCommand command = new SqlCommand("SELECT Nazwa FROM TYPY", _connection);
                _connection.Open();
                SqlDataReader sqlDataReader = command.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    TypCB.Items.Add(sqlDataReader["Nazwa".ToString()]);
                }
                sqlDataReader.Close();
            }
            _connection.Close();
        }

        private int Wstaw()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            _connection.Open();
            var wybrany = this.TypCB.SelectedItem;
            using (SqlCommand command = new SqlCommand("SELECT TypID FROM TYPY WHERE Nazwa = @wybrany"))
            {
                var DataSet = new DataSet();
                command.Parameters.AddWithValue("@wybrany", wybrany);
                command.Connection = _connection;
                command.ExecuteNonQuery();
                var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                dataAdapter.Fill(DataSet);
                dodajSamochod.TypLocal = Convert.ToInt32(DataSet.Tables[0].Rows[0]["TypID"]);
            }
            _connection.Close();

            string max = @"select MAX(PojazdID) FROM POJAZDY";
            _connection.Open();
            SqlCommand cmd = new SqlCommand(max, _connection);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                string d = dr[0].ToString();
                dodajSamochod.IdLocal = Convert.ToInt32(d) + 1;
            }
            _connection.Close();

            string command2 =
                @"INSERT INTO POJAZDY (PojazdID, Marka, Model, [Pojemnosc silnika], TypID) VALUES (@PojazdIDD, @Marka, @Model, @Pojemnosc, @typ)";
            using (SqlCommand command3 = new SqlCommand(command2))
            {
                command3.Parameters.AddWithValue("@PojazdIDD", dodajSamochod.IdLocal);
                command3.Parameters.AddWithValue("@Marka", dodajSamochod.MarkaLocal);
                command3.Parameters.AddWithValue("@Model", dodajSamochod.ModelLocal);
                command3.Parameters.AddWithValue("@Pojemnosc", (Double)dodajSamochod.Pojemnosc);
                command3.Parameters.AddWithValue("@typ", Convert.ToInt32(dodajSamochod.TypLocal));
                _connection.Open();
                command3.Connection = _connection;
                command3.ExecuteScalar();

                MessageBox.Show("Dodano pomyślnie!");
            }
            _connection.Close();
            return 0;
        }

        private void FillLB()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                string command3 = "SELECT Marka, Model, [Pojemnosc silnika] FROM POJAZDY";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command3, _connection);
                DataSet myDataSet = new DataSet();
                sqlDataAdapter.Fill(myDataSet, "POJAZDY");
                DataTable myDataTable = myDataSet.Tables[0];
                DataRow tempRow = null;

                foreach (DataRow tempRow_variable in myDataTable.Rows)
                {
                    tempRow = tempRow_variable;
                    SamochodyWBazieLB.Items.Add(
                        (tempRow["Marka"] + " " + tempRow["Model"] + " " + tempRow["Pojemnosc silnika"]));
                }
            }
            _connection.Close();
        }
    }
}