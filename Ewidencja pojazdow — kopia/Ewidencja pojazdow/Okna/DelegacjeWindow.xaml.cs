﻿using Ewidencja_pojazdow.Klasy;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for DelegacjeWindows.xaml
    /// </summary>
    public partial class DelegacjeWindows : Window
    {
        public DelegacjeWindows()
        {
            InitializeComponent();
            MiastaPosrednieTB.Text = "BRAK";
            Pobierz();
        }

        private DodajPracownika dodajPracownika = new DodajPracownika();
        private DataTable _SamochodyDataTable;
        private DataRow _tempRow = null;

        private void PracownikCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void MiastoPoczBT_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodajPracownika.MiastoPoczLocal = MiastoPoczBT.Text;
        }

        private void MiastoKoncBT_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodajPracownika.MiastoKoncLocal = MiastoKoncBT.Text;
        }

        private void PrzejechaneKmBT_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodajPracownika.PrzejechaneKm = Convert.ToInt32(PrzejechaneKmBT.Text);
        }

        private void DodajDelegacjeBT_Click(object sender, RoutedEventArgs e)
        {
            Dodaj();
        }

        private void SamochódLB_SelectionChanged()
        {
        }

        private void MiastaPosrednieTB_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MiastaPosrednieTB.Clear();
        }

        private void MiastaPosrednieTB_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MiastaPosrednieTB.Clear();
        }

        // ------ METODY ------
        private void Pobierz()
        {
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            using (_connection)
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Pracownicy", DBAccess._connection);

                SqlDataReader sqlDataReader = command.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    PracownikCB.Items.Add(sqlDataReader["Nazwisko".ToString()]);
                }
                sqlDataReader.Close();

                string command3 = "SELECT Marka, Model, [Pojemnosc silnika] FROM POJAZDY";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command3, DBAccess._connection);
                DataSet myDataSet = new DataSet();
                sqlDataAdapter.Fill(myDataSet, "POJAZDY");
                _SamochodyDataTable = myDataSet.Tables[0];

                foreach (DataRow tempRow_variable in _SamochodyDataTable.Rows)
                {
                    _tempRow = tempRow_variable;
                    SamochódLB.Items.Add(
                        (_tempRow["Marka"] + " " + _tempRow["Model"] + " " + _tempRow["Pojemnosc silnika"]));
                }
            }
        }

        private void Dodaj()
        {
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            var dataPowrotu = Convert.ToDateTime(DelegacjeCalendarPowrot.SelectedDate).ToString("yyyy-MM-dd");
            var dataWyjazdu = Convert.ToDateTime(DelegacjeCalendar.SelectedDate).ToString("yyyy-MM-dd");
            string miastoPocz = MiastoPoczBT.Text;
            string miastoKonc = MiastoKoncBT.Text;
            int kilometry = Convert.ToInt32(PrzejechaneKmBT.Text);
            string miastaPosrednie = MiastaPosrednieTB.Text;

            _connection.Open();
            var pracownik = PracownikCB.SelectedItem;
            int idTrasyNastępnej = 0;
            int idPracownika;
            int idPojazdu;
            // pobranie parametrów PracownikID i PojazdID z CB i LB
            string idPracownikaCommand = @"select PracownikID from PRACOWNICY where Nazwisko=@wybrany";
            using (SqlCommand command = new SqlCommand(idPracownikaCommand))
            {
                var DataSet = new DataSet();
                command.Parameters.AddWithValue("@wybrany", pracownik);
                command.Connection = _connection;
                command.ExecuteNonQuery();
                var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                dataAdapter.Fill(DataSet);
                idPracownika = Convert.ToInt32(DataSet.Tables[0].Rows[0]["PracownikID"]);
            }

            string IdPojazduCommand =
                @"select PojazdID from POJAZDY where Marka = @marka and Model = @model and [Pojemnosc silnika] = @pojemnosc";

            using (var command = new SqlCommand(IdPojazduCommand))
            {
                var DataSet = new DataSet();
                int index = SamochódLB.SelectedIndex;
                string selectedText = string.Empty;

                selectedText = SamochódLB.Items[index].ToString();

                string[] wybranysamochod = selectedText.Split(' ');

                command.Parameters.AddWithValue("@marka", wybranysamochod[0]);
                command.Parameters.AddWithValue("@model", wybranysamochod[1]);
                command.Parameters.AddWithValue("@pojemnosc", Convert.ToDouble(wybranysamochod[2]));
                command.Connection = _connection;
                command.ExecuteNonQuery();
                var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                dataAdapter.Fill(DataSet);
                idPojazdu = Convert.ToInt32(DataSet.Tables[0].Rows[0]["PojazdID"]);
            }
            // ----------------------------------------------------------------------

            string insertCommand =
                "insert into TRASY (PracownikID, PojazdID, [Data rozpoczecia trasy], [Data zakonczenia trasy], [Dlugosc trasy], [Miasto poczatkowe], [Miasto koncowe],[Miasta posrednie])" +
                "VALUES (@PracID, @PojID, @DataRoz, @DataZak, @DlTrasy, @MiastoPocz, @MiastoKonc, @MiastaPosd)";
            using (var command = new SqlCommand(insertCommand))
            {
                command.Parameters.AddWithValue("@PracID", idPracownika);
                command.Parameters.AddWithValue("@PojID", idPojazdu);
                command.Parameters.AddWithValue("@DataRoz", dataWyjazdu);
                command.Parameters.AddWithValue("@DataZak", dataPowrotu);
                command.Parameters.AddWithValue("@DlTrasy", kilometry);
                command.Parameters.AddWithValue("@MiastoPocz", miastoPocz);
                command.Parameters.AddWithValue("@MiastoKonc", miastoKonc);
                command.Parameters.AddWithValue("@MiastaPosd", miastaPosrednie);
                ;
                command.Connection = _connection;
                command.ExecuteScalar();
            }
            MessageBox.Show("Dodano pomyślnie!");
        }
    }
}