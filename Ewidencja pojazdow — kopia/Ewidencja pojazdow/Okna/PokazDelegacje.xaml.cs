﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for PokazDelegacje.xaml
    /// </summary>
    public partial class PokazDelegacje : Window
    {
        public PokazDelegacje()
        {
            InitializeComponent();
            WypelnijGrid();
        }

        private void OdswiezDG_Click(object sender, RoutedEventArgs e)
        {
            WypelnijGrid();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(DaneDoLink(DelegacjeGRID));
        }

        // ----- METODY -----

        private string miastoStartowe;
        private string miastoKoncowe;

        protected string[] splitedMiasta;

        private void WypelnijGrid()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select PRACOWNICY.Imie, PRACOWNICY.Nazwisko, POJAZDY.Marka, POJAZDY.Model, TYPY.Nazwa, TRASY.[Miasto poczatkowe], TRASY.[Miasto koncowe], TRASY.[Miasta posrednie] FROM TRASY join PRACOWNICY on TRASY.PracownikID = PRACOWNICY.PracownikID join POJAZDY on TRASY.PojazdID = POJAZDY.PojazdID join TYPY on POJAZDY.TypID = TYPY.TypID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                DelegacjeGRID.ItemsSource = dt.DefaultView;
            }
        }

        private string DaneDoLink(DataGrid grid)
        {
            char[] delete = { ',', ' ', ';', '.' };

            DataRowView row = grid.SelectedItem as DataRowView;
            miastoStartowe = row.Row.ItemArray[5].ToString();
            miastoKoncowe = row.Row.ItemArray[6].ToString();
            string link = "https://www.google.pl/maps/dir/" + miastoStartowe;
            if (row.Row.ItemArray[7].ToString() != "BRAK")
            {
                string miastaPosrednie = row.Row.ItemArray[7].ToString();
                for (int i = 0; i < miastaPosrednie.Length; i++)
                {
                    string[] s2 = miastaPosrednie.Split(delete, StringSplitOptions.RemoveEmptyEntries);
                    splitedMiasta = new string[s2.Length];
                    for (int j = 0; j < s2.Length; j++)
                    {
                        splitedMiasta[j] = s2[j];
                    }
                }

                for (int j = 0; j < splitedMiasta.Length; j++)
                {
                    link += "/" + splitedMiasta[j];
                }

                link += "/" + miastoKoncowe;
            }
            if (row.Row.ItemArray[7].ToString() == "BRAK")
            {
                link += "/" + miastoKoncowe;
            }

            return link;
        }
    }
}