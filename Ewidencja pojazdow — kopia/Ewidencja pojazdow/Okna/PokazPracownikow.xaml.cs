﻿using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for PokazPracownikow.xaml
    /// </summary>
    public partial class PokazPracownikow : Window
    {
        public PokazPracownikow()
        {
            InitializeComponent();
            Pobierz();
        }

        // ------ METODY ------
        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "SELECT dbo.PRACOWNICY.PracownikID, dbo.PRACOWNICY.Imie, dbo.PRACOWNICY.Nazwisko, dbo.PRACOWNICY.[Data urodzenia], ADRESY.Ulica, dbo.ADRESY.[Numer posiadlosci], dbo.ADRESY.[Numer lokalu], ADRESY.Miasto, dbo.ADRESY.Wojewodztwo, ADRESY.[Kod pocztowy], PRZELOZONY.Imie AS [Imie przelozonego], PRZELOZONY.Nazwisko AS [Nazwisko przelozonego]\r\nFROM dbo.PRACOWNICY\r\nLEFT OUTER JOIN dbo.PRACOWNICY AS PRZELOZONY ON PRACOWNICY.PrzelozonyID = PRZELOZONY.PracownikID\r\nJOIN dbo.ADRESY ON dbo.PRACOWNICY.AdresID = ADRESY.AdresID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                PracownicyGrid.ItemsSource = dt.DefaultView;
            }
        }
    }
}