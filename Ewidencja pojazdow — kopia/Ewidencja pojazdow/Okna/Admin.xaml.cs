﻿using System.Windows;
using System.Windows.Input;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void LoginTB_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            LoginTB.Clear();
        }

        private void HasloPB_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            HasloPB.Clear();
        }

        private void LoginBT_Click(object sender, RoutedEventArgs e)
        {
            cos();
        }

        private void cos()
        {
            UsuwanieWindow adminMode = new UsuwanieWindow();
            if ((HasloPB.Password == "password") && (LoginTB.Text == "admin"))
            {
                adminMode.Show();
                var myWindow = Admin.GetWindow(this);
                myWindow.Close();
            }
            else
            {
                MessageBox.Show("ERRRRROR");
                HasloPB.Clear();
                LoginTB.Clear();
            }
        }
    }
}