﻿using Ewidencja_pojazdow.Klasy;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for DodajPracownikaWindow.xaml
    /// </summary>
    public partial class DodajPracownikaWindow : Window
    {
        public DodajPracownikaWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private DodajPracownika dodaj = new DodajPracownika();

        private void NazwiskoTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.NazwiskoLocal = NazwiskoTB.Text;
        }

        private void UlicaTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.UlicaLocal = UlicaTB.Text;
        }

        private void NrBudynkuTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.NrBudynkuLocal = NrBudynkuTB.Text;
        }

        private void NrLokaluTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.NrLokaluLocal = NrLokaluTB.Text;
        }

        private void KodPocztowyTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.KodPocztowyLocal = KodPocztowyTB.Text;
        }

        private void MiastoTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.MiastoLocal = MiastoTB.Text;
        }

        private void WojewodztwoBT_TextChanged(object sender, TextChangedEventArgs e)
        {
            dodaj.WojewodztwoLocal = WojewodztwoBT.Text;
        }

        private void PrzelozonyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Dodaj();
        }

        // ------ METODY ------
        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Pracownicy", _connection);
                _connection.Open();
                SqlDataReader sqlDataReader = command.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    PrzelozonyComboBox.Items.Add(sqlDataReader["Nazwisko".ToString()]);
                }
                sqlDataReader.Close();
            }
            _connection.Close();
        }

        private void Dodaj()
        {
            int przelozonyID;
            var dataUrodzenia = Convert.ToDateTime(DataUrodzeniaKalendarz.SelectedDate).ToString("yyyy-MM-dd");
            int adresID;

            //wybranie z combobox, leci do zapytania, otrzymuje PracownikID dla Przelozonego
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            _connection.Open();
            var wybrany = PrzelozonyComboBox.SelectedItem;
            using (SqlCommand command = new SqlCommand("SELECT PracownikID FROM Pracownicy WHERE Nazwisko = @wybrany"))
            {
                var DataSet = new DataSet();
                command.Parameters.AddWithValue("@wybrany", wybrany);
                command.Connection = _connection;
                command.ExecuteNonQuery();
                var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                dataAdapter.Fill(DataSet);
                przelozonyID = Convert.ToInt32(DataSet.Tables[0].Rows[0]["PracownikID"]);
            }

            string insertAdres =
                @"insert into ADRESY (Ulica, [Numer posiadlosci], [Numer lokalu], [Miasto],[Wojewodztwo],[Kod pocztowy])
                              VALUES (@ulica, @nrpos, @nrlok, @miasto, @woj, @kod)";

            using (SqlCommand command = new SqlCommand(insertAdres))
            {
                command.Parameters.AddWithValue("@ulica", UlicaTB.Text);
                command.Parameters.AddWithValue("@nrpos", NrBudynkuTB.Text);
                command.Parameters.AddWithValue("@nrlok", NrLokaluTB.Text);
                command.Parameters.AddWithValue("@miasto", MiastoTB.Text);
                command.Parameters.AddWithValue("@woj", WojewodztwoBT.Text);
                command.Parameters.AddWithValue("@kod", KodPocztowyTB.Text);
                command.Connection = _connection;
                command.ExecuteScalar();
            }

            string selectAdresId = "select AdresID from ADRESY where Ulica = @ulica and [Numer posiadlosci] = @nrpos and [Numer lokalu] = @nrlok and [Miasto] = @miasto and [Wojewodztwo] = @woj and [Kod pocztowy] = @kod";
            using (SqlCommand command = new SqlCommand(selectAdresId))
            {
                var DataSet = new DataSet();
                command.Parameters.AddWithValue("@ulica", UlicaTB.Text);
                command.Parameters.AddWithValue("@nrpos", NrBudynkuTB.Text);
                command.Parameters.AddWithValue("@nrlok", NrLokaluTB.Text);
                command.Parameters.AddWithValue("@miasto", MiastoTB.Text);
                command.Parameters.AddWithValue("@woj", WojewodztwoBT.Text);
                command.Parameters.AddWithValue("@kod", KodPocztowyTB.Text);
                command.Connection = _connection;
                command.ExecuteNonQuery();

                var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                dataAdapter.Fill(DataSet);

                adresID = Convert.ToInt32(DataSet.Tables[0].Rows[0]["AdresID"]);
            }

            string insertPracownik =
                "insert into  PRACOWNICY (Imie, Nazwisko, [Data urodzenia], PrzelozonyID, AdresID) VALUES (@imie, @naz,@data,@prze,@adres)";

            using (SqlCommand command = new SqlCommand(insertPracownik))
            {
                command.Parameters.AddWithValue("@imie", ImieTB.Text);
                command.Parameters.AddWithValue("@naz", NazwiskoTB.Text);
                command.Parameters.AddWithValue("@data", dataUrodzenia);
                command.Parameters.AddWithValue("@prze", przelozonyID);
                command.Parameters.AddWithValue("@adres", adresID);
                command.Connection = _connection;
                command.ExecuteNonQuery();
            }

            MessageBox.Show("Dodano pomyślnie!");
        }
    }
}