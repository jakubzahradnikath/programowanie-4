﻿using System;

namespace Ewidencja_pojazdow.Klasy
{
    internal class DodajPracownika
    {
        // -------------------------------

        public string ImieLocal;
        public string NazwiskoLocal;
        public string UlicaLocal;
        public string NrBudynkuLocal;
        public string NrLokaluLocal;
        public string WojewodztwoLocal;
        public string MiastoLocal;
        public string KodPocztowyLocal;
        public string DataUrodzeniaLocal;
        public Object PrzelozonyIdLocal;
        public Object AdresIdLocal;
        public string UlicaPoczLocal;
        public string UlicaKoncLocal;
        public string MiastoPoczLocal;
        public string MiastoKoncLocal;

        public int PrzejechaneKm;

        public string DataWyjazdy;
        // -------------------------------
    }
}