﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Ewidencja_pojazdow.Okna_usuwania
{
    /// <summary>
    /// Interaction logic for UsunNaprawy.xaml
    /// </summary>
    public partial class UsunNaprawy : Window
    {
        public UsunNaprawy()
        {
            InitializeComponent();
            Pobierz();
        }

        private void UsunBT_Click(object sender, RoutedEventArgs e)
        {
            Usun();
        }

        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select POJAZDY.PojazdID, POJAZDY.Marka, POJAZDY.Model, [HISTORIE NAPRAW].[Opis naprawy], [HISTORIE NAPRAW].[Koszt naprawy], [HISTORIE NAPRAW].[Data naprawy], [HISTORIE NAPRAW].[Przebieg w czasie naprawy] from [HISTORIE NAPRAW] join POJAZDY on [HISTORIE NAPRAW].PojazdID = POJAZDY.PojazdID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                NaprawyDG.ItemsSource = dt.DefaultView;
            }
        }

        private void Usun()
        {
            int usun = 0;
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                DataRowView row = NaprawyDG.SelectedItem as DataRowView;
                usun = Convert.ToInt32(row.Row.ItemArray[0]);

                string deleteCommand = "delete from [HISTORIE NAPRAW] where NaprawaID=@naprawa";

                using (SqlCommand command = new SqlCommand(deleteCommand))
                {
                    command.Parameters.AddWithValue("@naprawa", usun);
                    command.Connection = _connection;
                    command.ExecuteScalar();
                }

                NaprawyDG.ItemsSource = null;
                Pobierz();

                MessageBox.Show("USUNIĘTO");
            }
        }
    }
}