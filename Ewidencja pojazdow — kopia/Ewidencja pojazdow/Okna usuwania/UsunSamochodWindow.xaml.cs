﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Ewidencja_pojazdow.Okna_usuwania
{
    /// <summary>
    /// Interaction logic for UsunSamochodWindow.xaml
    /// </summary>
    public partial class UsunSamochodWindow : Window
    {
        public UsunSamochodWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private void UsunBT_Click(object sender, RoutedEventArgs e)
        {
            Usun();
        }

        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select POJAZDY.PojazdID, POJAZDY.Marka, POJAZDY.Model, POJAZDY.[Pojemnosc silnika], TYPY.Nazwa from POJAZDY join TYPY on POJAZDY.TypID = TYPY.TypID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                SamochodyDG.ItemsSource = dt.DefaultView;
            }
        }

        private void Usun()
        {
            int usun = 0;
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                DataRowView row = SamochodyDG.SelectedItem as DataRowView;
                usun = Convert.ToInt32(row.Row.ItemArray[0]);

                string deleteCommand = "delete from POJAZDY where PojazdID=@id";

                using (SqlCommand command = new SqlCommand(deleteCommand))
                {
                    command.Parameters.AddWithValue("@id", usun);
                    command.Connection = _connection;
                    command.ExecuteScalar();
                }

                SamochodyDG.ItemsSource = null;
                Pobierz();

                MessageBox.Show("USUNIĘTO");
            }
        }
    }
}