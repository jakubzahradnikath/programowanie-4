﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Ewidencja_pojazdow.Okna_usuwania
{
    /// <summary>
    /// Interaction logic for UsunPracownikaWindow.xaml
    /// </summary>
    public partial class UsunPracownikaWindow : Window
    {
        public UsunPracownikaWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private void UsunBT_Click(object sender, RoutedEventArgs e)
        {
            Usun();
        }

        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection =
            new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "SELECT dbo.PRACOWNICY.PracownikID, dbo.ADRESY.AdresID, dbo.PRACOWNICY.Imie, dbo.PRACOWNICY.Nazwisko, dbo.PRACOWNICY.[Data urodzenia], ADRESY.Ulica, dbo.ADRESY.[Numer posiadlosci], dbo.ADRESY.[Numer lokalu], ADRESY.Miasto, dbo.ADRESY.Wojewodztwo, ADRESY.[Kod pocztowy], PRZELOZONY.Imie AS [Imie przelozonego], PRZELOZONY.Nazwisko AS [Nazwisko przelozonego]\r\nFROM dbo.PRACOWNICY\r\nLEFT OUTER JOIN dbo.PRACOWNICY AS PRZELOZONY ON PRACOWNICY.PrzelozonyID = PRZELOZONY.PracownikID\r\nJOIN dbo.ADRESY ON dbo.PRACOWNICY.AdresID = ADRESY.AdresID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                PracownicyDG.ItemsSource = dt.DefaultView;
            }
        }

        private void Usun()
        {
            int usun = 0;
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection =
            new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                DataRowView row = PracownicyDG.SelectedItem as DataRowView;
                usun = Convert.ToInt32(row.Row.ItemArray[0]);

                string deletePracownikCommand = "delete w from PRACOWNICY w join ADRESY on w.AdresID = ADRESY.AdresID\r\nwhere w.PracownikID = @prac";

                using (SqlCommand command = new SqlCommand(deletePracownikCommand))
                {
                    command.Parameters.AddWithValue("@prac", usun);
                    command.Connection = _connection;
                    command.ExecuteScalar();
                }

                PracownicyDG.ItemsSource = null;
                Pobierz();

                MessageBox.Show("USUNIĘTO");
            }
        }
    }
}