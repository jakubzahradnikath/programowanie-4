﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow.Okna_usuwania
{
    /// <summary>
    /// Interaction logic for UsunDelegacjeWindow.xaml
    /// </summary>
    public partial class UsunDelegacjeWindow : MetroWindow
    {
        private SqlCommandBuilder scb;
        private SqlDataAdapter sdr;
        private DataTable dt;

        public UsunDelegacjeWindow()
        {
            InitializeComponent();
            WypelnijGrid();
        }

        private void DelegacjeDG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void ModyfikujBT_Click(object sender, RoutedEventArgs e)
        {
            Usun();
        }

        private void WypelnijGrid()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select TRASY.TrasaID, PRACOWNICY.Imie, PRACOWNICY.Nazwisko, POJAZDY.Marka, POJAZDY.Model, TYPY.Nazwa, TRASY.[Miasto poczatkowe], TRASY.[Miasto koncowe], TRASY.[Miasta posrednie] FROM TRASY join PRACOWNICY on TRASY.PracownikID = PRACOWNICY.PracownikID join POJAZDY on TRASY.PojazdID = POJAZDY.PojazdID join TYPY on POJAZDY.TypID = TYPY.TypID";
                SqlCommand show = new SqlCommand(command3, _connection);
                sdr = new SqlDataAdapter(show);
                dt = new DataTable();
                sdr.Fill(dt);
                DelegacjeDG.ItemsSource = dt.DefaultView;
            }
        }

        private void Usun()
        {
            int usun = 0;
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                DataRowView row = DelegacjeDG.SelectedItem as DataRowView;
                usun = Convert.ToInt32(row.Row.ItemArray[0]);

                string deleteCommand = "delete from TRASY where TrasaID=@trasa";

                using (SqlCommand command = new SqlCommand(deleteCommand))
                {
                    command.Parameters.AddWithValue("@trasa", usun);
                    command.Connection = _connection;
                    command.ExecuteScalar();
                }

                DelegacjeDG.ItemsSource = null;
                WypelnijGrid();

                MessageBox.Show("USUNIĘTO");
            }
        }
    }
}