﻿using Ewidencja_pojazdow.Okna_usuwania;
using System.Windows;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for UsuwanieWindow.xaml
    /// </summary>
    public partial class UsuwanieWindow : MetroWindow
    {
        public UsuwanieWindow()
        {
            InitializeComponent();
        }

        private void UsunPracownikaBT_Click(object sender, RoutedEventArgs e)
        {
            UsunPracownikaWindow usun = new UsunPracownikaWindow();
            usun.Show();
        }

        private void UsunSamochodBT_Click(object sender, RoutedEventArgs e)
        {
            UsunSamochodWindow usun = new UsunSamochodWindow();
            usun.Show();
        }

        private void UsunNaprawyBT_Copy_Click(object sender, RoutedEventArgs e)
        {
            UsunNaprawy usun = new UsunNaprawy();
            usun.Show();
        }

        private void UsunDelegacjeBT_Click(object sender, RoutedEventArgs e)
        {
            UsunDelegacjeWindow usun = new UsunDelegacjeWindow();
            usun.Show();
        }
    }
}