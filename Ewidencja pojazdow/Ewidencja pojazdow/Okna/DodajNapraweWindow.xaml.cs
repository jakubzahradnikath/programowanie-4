﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for DodajNapraweWindow.xaml
    /// </summary>
    public partial class DodajNapraweWindow : MetroWindow
    {
        public DodajNapraweWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void DodajBT_Click(object sender, RoutedEventArgs e)
        {
            Dodaj();
        }

        // ------ METODY ------
        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                string command3 = "SELECT Marka, Model, [Pojemnosc silnika] FROM POJAZDY";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command3, _connection);
                DataSet myDataSet = new DataSet();
                sqlDataAdapter.Fill(myDataSet, "POJAZDY");
                DataTable _SamochodyDataTable = myDataSet.Tables[0];
                DataRow _tempRow = null;

                foreach (DataRow tempRow_variable in _SamochodyDataTable.Rows)
                {
                    _tempRow = tempRow_variable;
                    ListaSamochodowLB.Items.Add(
                        (_tempRow["Marka"] + " " + _tempRow["Model"] + " " + _tempRow["Pojemnosc silnika"]));
                }
            }
        }

        private void Dodaj()
        {
            int przebieg = Convert.ToInt32(PrzebiegTB.Text);
            int koszt = Convert.ToInt32(KosztTB.Text);
            string opis = OpisNaprawyTB.Text;
            string data = Convert.ToDateTime(DataNaprawyKalendarz.SelectedDate).ToString("yyyy-MM-dd");
            int samochodId = 0;

            string tempCar = ListaSamochodowLB.SelectedItem.ToString();
            string[] splitSamochodu = tempCar.Split(' ');
            string marka = splitSamochodu[0];
            string model = splitSamochodu[1];
            string pojemnosc = splitSamochodu[2];

            string samochodIdCommand =
                @"select PojazdID from POJAZDY where Marka = @marka and Model = @model and [Pojemnosc silnika] = @pojemnosc";
            string insertCommand = @"insert into [HISTORIE NAPRAW]([Data naprawy], [Opis naprawy], [Przebieg w czasie naprawy], [Koszt naprawy], PojazdID) values (@Data, @Opis, @Przebieg, @Koszt, @PojazdID)";

            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                using (SqlCommand command = new SqlCommand(samochodIdCommand))
                {
                    var DataSet = new DataSet();
                    command.Parameters.AddWithValue("@marka", marka);
                    command.Parameters.AddWithValue("@model", model);
                    command.Parameters.AddWithValue("@pojemnosc", Convert.ToDouble(pojemnosc));
                    command.Connection = _connection;
                    _connection.Open();
                    command.ExecuteNonQuery();
                    var dataAdapter = new SqlDataAdapter { SelectCommand = command };
                    dataAdapter.Fill(DataSet);
                    samochodId = Convert.ToInt32(DataSet.Tables[0].Rows[0]["PojazdID"]);
                }
                using (SqlCommand command3 = new SqlCommand(insertCommand))
                {
                    command3.Parameters.AddWithValue("@Data", data);
                    command3.Parameters.AddWithValue("@Opis", opis);
                    command3.Parameters.AddWithValue("@Przebieg", przebieg);
                    command3.Parameters.AddWithValue("@Koszt", koszt);
                    command3.Parameters.AddWithValue("@PojazdID", samochodId);
                    command3.Connection = _connection;
                    command3.ExecuteScalar();
                }
                MessageBox.Show("Dodano pomyślnie!");
            }
        }
    }
}