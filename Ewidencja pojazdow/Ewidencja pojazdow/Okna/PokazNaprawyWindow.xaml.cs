﻿using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Interaction logic for PokazNaprawyWindow.xaml
    /// </summary>
    public partial class PokazNaprawyWindow : MetroWindow
    {
        public PokazNaprawyWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        // ----- METODY ------

        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select POJAZDY.Marka, POJAZDY.Model, [HISTORIE NAPRAW].[Opis naprawy], [HISTORIE NAPRAW].[Koszt naprawy], [HISTORIE NAPRAW].[Data naprawy], [HISTORIE NAPRAW].[Przebieg w czasie naprawy] from [HISTORIE NAPRAW] join POJAZDY on [HISTORIE NAPRAW].PojazdID = POJAZDY.PojazdID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                NaprawyGRID.ItemsSource = dt.DefaultView;
            }
        }
    }
}