﻿using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow.Okna
{
    /// <summary>
    /// Logika interakcji dla klasy PokazSamochodyWindow.xaml
    /// </summary>
    public partial class PokazSamochodyWindow : MetroWindow
    {
        public PokazSamochodyWindow()
        {
            InitializeComponent();
            Pobierz();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        // ------ METODY ------
        private void Pobierz()
        {
            SqlConnection _connection = new SqlConnection(@"Data Source = ewidencja.database.windows.net; Initial Catalog = P4Projekt; Integrated Security = False; User ID = kuba; Password=z1emn1@k1s@bosk1E; Connect Timeout = 30; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //SqlConnection _connection = new SqlConnection(@"Data Source=KUBA-LAP; Initial Catalog=P4_ew_pojazdow; Integrated Security=True");
            using (_connection)
            {
                _connection.Open();
                string command3 =
                    "select POJAZDY.PojazdID, POJAZDY.Marka, POJAZDY.Model, POJAZDY.[Pojemnosc silnika], TYPY.Nazwa from POJAZDY join TYPY on POJAZDY.TypID = TYPY.TypID";
                SqlCommand show = new SqlCommand(command3, _connection);
                SqlDataAdapter sdr = new SqlDataAdapter(show);
                DataTable dt = new DataTable();
                sdr.Fill(dt);
                PojazdyDG.ItemsSource = dt.DefaultView;
            }
        }
    }
}