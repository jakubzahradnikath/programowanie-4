﻿using Ewidencja_pojazdow.Klasy;
using Ewidencja_pojazdow.Okna;
using System.Windows;
using MahApps.Metro;
using MahApps.Metro.Controls;

namespace Ewidencja_pojazdow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DBAccess._connection.Open();
        }

        public Admin admin = new Admin();

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DodajPracownikaWindow DodajPracownikaWindow = new DodajPracownikaWindow();
            DodajPracownikaWindow.Show();
        }

        private void DodajDelegacjeButton_Click(object sender, RoutedEventArgs e)
        {
            DelegacjeWindows delegacje = new DelegacjeWindows();
            delegacje.Show();
        }

        private void WyswietlDelegacjeButton_Click(object sender, RoutedEventArgs e)
        {
            PokazDelegacje pokazDelegacje = new PokazDelegacje();
            pokazDelegacje.Show();
        }

        private void WyswietlPracownikowButton_Click(object sender, RoutedEventArgs e)
        {
            PokazPracownikow pokazPracownikow = new PokazPracownikow();
            pokazPracownikow.Show();
        }

        private void DodajSamochodButton_Click(object sender, RoutedEventArgs e)
        {
            DodajSamochod dodajSamochod = new DodajSamochod();
            dodajSamochod.Show();
        }

        private void DodajNapraweBT_Click(object sender, RoutedEventArgs e)
        {
            DodajNapraweWindow dodaj = new DodajNapraweWindow();
            dodaj.Show();
        }

        private void WyswietlSamochodyBT_Click(object sender, RoutedEventArgs e)
        {
            PokazSamochodyWindow pokaz = new PokazSamochodyWindow();
            pokaz.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PokazNaprawyWindow pokaz = new PokazNaprawyWindow();
            pokaz.Show();
        }

        private void AdminBT_Click(object sender, RoutedEventArgs e)
        {
            admin.Show();
        }
    }
}